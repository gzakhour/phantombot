# PhantomBot
Built on top of [PhantomJS](http://phantomjs.org). PhantomBot makes single page
applications behave like multi page ones. PhantomBot is like a proxy server
over a single domain only. PhantomBot can also cache files if explicitly 
stated. This is ideal for crawling bots like Google's search bot.


# Getting Started

## Installing the dependencies
Start by installing all the requirements through composer

```
$ composer install
```

**PS** Make sure that the `bin/` folder contains two binaries, `phantomjs` and
`phantomloader`. If one of them is not found then copy it from `vendor/bin` to
`bin/`.

## Configuration
You can find the configuration file in `app/config.php`. The configuration
variables are explained here:

* `port`     The port on which to run PhantomBot. Default is `6969`
* `base_url` The base URL of the application, example: `http://google.com`, be
   careful not to include a / at the end of the URL.
* `cache.enable` Whether PhantomBot should cache pages or not. Default is `true`
* `cache.dir` The directory in which the cached files should be stored in.
  Default is `./cache`
* `cache.files` An array of the files to cache without their base URL. For example
  if you wish to cache `http://google.com/images/srpr/logo11w.png` then you would
  set the `base_url` to `http://google.com/` and include `/images/srpr/logo11w.png`
  in the `cache.files` array.
* `benchmark.enable` whether benchmarking should be used or not. Default is `true`
* `benchmark.dir` the directory in which to write the `benchmark.log` file.
  Default is `./log`

## Running PhantomBot
Execute the `start` executable and let it be. Then open your browser to
[http://localhost:6969](http://localhost:6969) and you can use it.

# Benchmarks
Benchmarks are used to provide statistics about how much time it took to access
and render a certain page. If the configuration has not been changed you will
find the benchmarks in `./log/benchmarks.log`. Each line represents a request.
The first and second token represents the source of the content

* `>> [Server]` means that the page was fetched from the remote server
* `>> [Cache]` means that the page was served from cache. This happens when there
  is a cache-hit.
* `<< [Cache]` means that the page was fetched from the remote server and cached.
  This happens when there is a cache-miss.

The paranthesised number is the HTTP code of the request done by PhantomBot.
Followed by the HTTP method and the URL. The last token is how much time it
took to access and render the page in seconds.

An example log is:
```
>> [Server]	(200) GET	http://localhost/frontend/		0.31712198257446s
>> [Server]	(200) GET	http://localhost/frontend/static/css/bootstrap.min.css		0.24317908287048s
>> [Server]	(200) GET	http://localhost/frontend/static/js/handlebars-v1.3.0.js		0.23244380950928s
<< [Cache]	(200) GET	http://localhost/frontend/static/img/logo.png		0.17776703834534s
>> [Server]	(200) GET	http://localhost/frontend/static/js/jquery.min.js		0.21577906608582s
>> [Server]	(200) GET	http://localhost/frontend/static/js/bootstrap.min.js		0.19963479042053s
>> [Server]	(200) GET	http://localhost/frontend/static/js/router.js		0.17505097389221s
>> [Server]	(404) GET	http://localhost/static/fonts/9k-RPmcnxYEPm8CNFsH2gg.woff		0.17447900772095s
>> [Server]	(200) GET	http://localhost/frontend/templates/index.tpl		0.17865896224976s
>> [Server]	(200) GET	http://localhost/frontend/php/latest.php		0.19745588302612s
>> [Server]	(200) GET	http://localhost/frontend/		0.29560589790344s
>> [Server]	(200) GET	http://localhost/frontend/static/css/bootstrap.min.css		0.20186305046082s
>> [Server]	(200) GET	http://localhost/frontend/static/js/handlebars-v1.3.0.js		0.20827698707581s
>> [Cache]	(200) GET	http://localhost/frontend/static/img/logo.png		0.00020217895507812s
>> [Server]	(200) GET	http://localhost/frontend/static/js/jquery.min.js		0.24898409843445s
>> [Server]	(200) GET	http://localhost/frontend/static/js/bootstrap.min.js		0.19218397140503s
>> [Server]	(200) GET	http://localhost/frontend/static/js/router.js		0.17331790924072s
>> [Server]	(404) GET	http://localhost/static/fonts/9k-RPmcnxYEPm8CNFsH2gg.woff		0.17250990867615s
>> [Server]	(200) GET	http://localhost/frontend/templates/index.tpl		0.16631984710693s
>> [Server]	(200) GET	http://localhost/frontend/php/latest.php		0.17039799690247s
>> [Cache]	(200) GET	http://localhost/frontend/static/img/logo.png		0.00018596649169922s
>> [Server]	(200) GET	http://localhost/frontend/static/img/logo.png?version=1		0.22905015945435s
```
Where `/frontend/static/img/logo.png` is inside the `cache.files` array.

# Cache
Managing cache is done through the `cache-manager` executable. It one of 3 parameters.

```
$ cache-manager -h
```
Displays the help message


```
$ cache-manager -c
```
Clears the cache by removing the cache folder


```
$ cache-manager -r
```
Re-caches all the files by accessing them through PhantomBot again.

# Deploying on Nginx

The deployment on Nginx is very simple. Homever you have to make sure that you
have `php` and `php-fpm` installed. To execute every `php` script proceed to
include in your `server` block:

```
location ~ \.php$ {
    root           /usr/share/nginx/html;
    index          index.php;
    fastcgi_pass   unix:/run/php-fpm/php-fpm.sock;
    include        fastcgi.conf;
}
```

Moreover to make PhantomBot work we must redirect every request to `app/_app_.php`
and preserve the `REQUEST_URI`. This can be achieved by adding to the `location /`
block in your `server` block  the following:

```
rewrite ^/.*$ /app/_app_.php last;*
```

**PS** As a result any file that is accessed and exists, for example `vendor/autoloader.php`
will not be redircted and will instead load `vendor/autoloader.php`.

## Fix for the above PS
Edit the `location` for `/` to something like this
```
location / {
    root   /usr/share/nginx/html/app;
    index  index.html index.htm;
    rewrite ^/(.*)$ /_app_.php last;
}
```

Also edit the `location` block of the PHP to something like this

```
location ~ \.php$ {
    root           /usr/share/nginx/html/app;
    index          index.php;
    fastcgi_pass   unix:/run/php-fpm/php-fpm.sock;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    include        fastcgi.conf;
    try_files      $uri @missing;

}

location @missing {
    rewrite ^/(.*)$ /_app_.php last;
}
```

Now the following URLs will not be redirected:

* `/_app_.php`
* `/_config_.php`
* `/_helpers_.php`


# Forward Bots

Assuming this application is running on port `8000` on `localhost` then to forward
the bots from `localhost:80` to `localhost:8000` then in the `server` block of
the `localhost:80` include in the `location /` block the following piece of code
in the beginning

```
if ($http_user_agent ~* (bot|spider|crawler|sniffer|facebook|google|yahoo|hoot) ) {
    rewrite ^ $scheme://localhost:8000$request_uri last;
}
```
This will redirect any user agent containing the words bot, spider, crawler, sniffer,
facebook, google, yahoo or hoot in them. The following services as of July 2015
were tested to be working:

* Google
* Facebook
* Twitter
* Google+
* LinkedIn
* DuckDuckGo
* Yahoo
* Bing
* Hootsuite
* Bit.ly

# License
Refer to the `LICENSE` file.
