<?php

chdir('../');

require './vendor/autoload.php';
require './app/_config_.php';
require './app/_helpers_.php';

use JonnyW\PhantomJs\Client;
use JonnyW\PhantomJs\DependencyInjection\ServiceContainer;

session_start();

/**
 * Given a URL and a method, this function uses PhantomJS to render the webpage
 * and returns an array, the first element being the status of the http reply
 * from the remote server and the second is the parsed content
*/
function run($url, $method) {
    $client = Client::getInstance();
    $client->addOption("--cookies-file=/tmp/".sha1(session_id()).".txt");

    $request  = $client->getMessageFactory()->createRequest();
    $request->setUrl($url);
    $request->setMethod($method);
    $request->setRequestData($_POST);
    $request->setTimeout(100000); // increase the timeout to 100 seconds

    $response = $client->getMessageFactory()->createResponse();

    $client->send($request, $response);

    $status = $response->getStatus();
    $content = "";

    if($status === 200 || $status === 301) {
        $type = $response->getHeader("Content-Type");
        if(substr($type, 0, 9) === "text/html") {
            $content = $response->getContent();
        } else {
            header("Content-Type: $type");
            $content = file_get_contents($url);
        }
    } else {
        http_response_code($response->getStatus());
        $content = $response->getContent();
    }

    return array($status, $content);
}


$url = url();
$method = method();

// start benchmarking with current time
$benchmark = logBenchmark(microtime(TRUE));
$reqMethod = "";

if(isCached()) { // serve the file if it's cached
    echo file_get_contents(cacheFile());
    $reqMethod = ">> [Cache]";
    $status = 200;
} else { // get it from the server
    $content = run($url, $method);
    $status  = $content[0];
    $content = $content[1];

    echo $content;
    $reqMethod = ">> [Server]";

    if(shouldCache()) { // if it should be cached then cache it
        if(!file_exists(cacheFileDir()))
            mkdir(cacheFileDir(), 0777, true);

        $file = fopen(cacheFile(), "w+");
        fwrite($file, $content);
        fclose($file);

        $reqMethod = "<< [Cache]";
    }
}

$logger = $benchmark(microtime(TRUE)); // stop the benchmarking with current time
$logger($reqMethod, $url, $method, $status); // write the parameters to the log
