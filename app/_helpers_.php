<?php

require "./app/_config_.php";

/**
 * Get the remote URL of the current request
 *
 * @return string       the base url with the request uri
*/
function url() {
    return config('base_url').$_SERVER["REQUEST_URI"];
}

/**
 * Get the method of the current request
 *
 * @return string       the method, if empty then return GET
*/
function method() {
    if(isset($_SERVER["REQUEST_METHOD"])) {
        return $_SERVER["REQUEST_METHOD"];
    } else {
        return 'GET';
    }
}

/**
 * An abstraction on top of the $config variable in the config.php file
 * Concatenate the different layers of the config with .
 * example config('cache.files')
 *
 * @return      the respective value of $config
*/
function config($key) {
    global $config;

    $keys = explode('.', $key);
    $c = $config;
    foreach($keys as $k) {
        if($k != null)
            $c = $c[$k];
    }

    return $c;
}

/**
 * The benchmarking function. Execute it before the script with the current time
 * as parameter, this returns a function which should be called with the current
 * time also. The later also returns a functino which should be given 4 strings
 * to write to the log file
*/
function logBenchmark($start) {
    return function($end) use ($start){
        return function($reqMethod, $url, $method, $status) use ($start, $end) {
            if(!config("benchmark.enable")) {
                return;
            }

            if(!file_exists(config('benchmark.dir'))) {
                mkdir(config('benchmark.dir'), 0777, true);
            }

            $benchmark = fopen(config("benchmark.dir")."/benchmark.log", "a");
            fwrite($benchmark, "$reqMethod\t($status) $method\t$url\t\t".($end-$start)."s\n");
            fclose($benchmark);
        };
    };
}

/**
 * @return bool     whether the current file should be cached or not
*/
function shouldCache() {
    $cache = false;
    $files = config('cache.files');
    for($i=0;$i<count($files);$i++) {
        $cache = $cache || (preg_match($files[$i], $_SERVER["REQUEST_URI"]) == 1);
    }
    return config('cache.enable') && $cache;
}

/**
 * @return bool     whether the current file is cached or not
*/
function isCached() {
    return shouldCache() && file_exists(config('cache.dir').$_SERVER["REQUEST_URI"]);
}

/**
 * @return string   the location of the cached version of the URL
*/
function cacheFile() {
    return config('cache.dir').$_SERVER["REQUEST_URI"];
}

/**
 * @return string   the folder in which the cached file lives in
*/
function cacheFileDir() {
    $paths = explode('/', $_SERVER["REQUEST_URI"]);
    return config('cache.dir').'/'.join('/', array_slice($paths, 0, count($paths)-1));   
}
