<?php

$config = [

    'port' => 6969,

    // This is the base URL, should not end with /
    // all requests will go to this link
    'base_url' => 'http://localhost',

    // the cache settings
    'cache' => [

        // to disable caching, set this value to false
        'enable' => true,

        // Where the cached files should be stored
        'dir'    => './cache',

        // All URLs that should be cached should be listed here,
        // obviously stripped from the base_url
        // the URLs should be presented as a regex, for example the
        // bellow expressions will cache every css file in a static/css
        // and every js file in a static/js file and everything in static/img
        // no matter where this static folder is
        'files' => [
            '/.*\/static\/css\/.*\.css.*/',
            '/.*\/static\/js\/.*\.js/',
            '/.*\/static\/img\/.*/'
        ]
    ],

    // benchmarking settings
    'benchmark' => [

        // to disable benchmarking set this value to false
        'enable' => true,

        // directory to write the benchmark.log file to, without a / at the end
        'dir'    => './log'
    ]

];
